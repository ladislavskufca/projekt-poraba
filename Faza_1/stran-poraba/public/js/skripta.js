function izbiraZnamke() {

  var izbira = document.getElementById("izbira_znamke");
  var izbranaZnamka = izbira.options[izbira.selectedIndex].text;

  brisiVseModele();

  if (izbranaZnamka == "Alfa Romeo") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="33">33</option><option value="75">75</option><option value="90">90</option><option value="145">145</option><option value="146">146</option><option value="147">147</option><option value="155">155</option><option value="156">156</option><option value="159">159</option><option value="164">164</option><option value="166">166</option><option value="4C">4C</option><option value="8C">8C</option><option value="1750">1750</option><option value="Alfasud">Alfasud</option><option value="Alfetta">Alfetta</option><option value="Brera">Brera</option><option value="Crosswagon">Crosswagon</option><option value="Giulia">Giulia</option><option value="Giulietta">Giulietta</option><option value="GT">GT</option><option value="GTV">GTV</option><option value="MiTo">MiTo</option><option value="RZ/SZ">RZ/SZ</option><option value="Spider">Spider</option><option value="Sprint">Sprint</option></select>';
  }

  else if (izbranaZnamka == "Aston Martin") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Cygnet">Cygnet</option><option value="DB">DB</option><option value="DB7">DB7</option><option value="DB9">DB9</option><option value="DBS">DBS</option><option value="Lagonda">Lagonda</option><option value="One-77">One-77</option><option value="Rapide">Rapide</option><option value="V8">V8</option><option value="Vantage">Vantage</option><option value="Vanquish">Vanquish</option><option value="Virage">Virage</option><option value="Volante">Volante</option><option value="Zagato">Zagato</option></select>';
  }

  else if (izbranaZnamka == "Audi") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="50">50</option><option value="60">60</option><option value="80">80</option><option value="90">90</option><option value="100">100</option><option value="200">200</option><option value="A1">A1</option><option value="A2">A2</option><option value="A3">A3</option><option value="A4">A4</option><option value="A5">A5</option><option value="A6">A6</option><option value="A7">A7</option><option value="A8">A8</option><option value="Allroad">Allroad</option><option value="Coupe">Coupe</option><option value="Cabriolet">Cabriolet</option><option value="Q1">Q1</option><option value="Q3">Q3</option><option value="Q5">Q5</option><option value="Q7">Q7</option><option value="R8">R8</option><option value="RS2">RS2</option><option value="RS3">RS3</option><option value="RS4">RS4</option><option value="RS5">RS5</option><option value="RS6">RS6</option><option value="RS7">RS7</option><option value="RS Q3">RS Q3</option><option value="S1">S1</option><option value="S2">S2</option><option value="S3">S3</option><option value="S4">S4</option><option value="S5">S5</option><option value="S6">S6</option><option value="S7">S7</option><option value="S8">S8</option><option value="SQ5">SQ5</option><option value="TT">TT</option><option value="V8">V8</option></select>';
  }

  else if (izbranaZnamka == "Bentley") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Arnage">Arnage</option><option value="Azure">Azure</option><option value="Bentayga">Bentayga</option><option value="Brooklands">Brooklands</option><option value="Continental">Continental</option><option value="Eight">Eight</option><option value="Mulsanne">Mulsanne</option><option value="Turbo">Turbo</option></select>';
  }

  else if (izbranaZnamka == "BMW") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="1802">1802</option><option value="2002">2002</option><option value="i3">i3</option><option value="i8">i8</option><option value="M1">M1</option><option value="M2">M2</option><option value="M3">M3</option><option value="M4">M4</option><option value="M5">M5</option><option value="M6">M6</option><option value="serija 1:">Serija 1</option><option value="serija 2:">Serija 2</option><option value="serija 3:">Serija 3</option><option value="serija 4:">Serija 4</option><option value="serija 5:">Serija 5</option><option value="serija 6:">Serija 6</option><option value="serija 7:">Serija 7</option><option value="serija 8:">Serija 8</option><option value="serija X1:">Serija X1</option><option value="serija X3:">Serija X3</option><option value="serija X4:">Serija X4</option><option value="serija X5:">Serija X5</option><option value="serija X6:">Serija X6</option><option value="Z1">Z1</option><option value="Z3">Z3</option><option value="Z4">Z4</option><option value="Z8">Z8</option></select>';
  }

  else if (izbranaZnamka == "Cadillac") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Allante">Allante</option><option value="BLS">BLS</option><option value="Deville">Deville</option><option value="CTS">CTS</option><option value="Eldorado">Eldorado</option><option value="Escalade">Escalade</option><option value="Fleetwood">Fleetwood</option><option value="Seville">Seville</option><option value="SRX">SRX</option><option value="STS">STS</option><option value="XLR">XLR</option></select>';
  }

  else if (izbranaZnamka == "Chevrolet") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Astro">Astro</option><option value="Avalanche">Avalanche</option><option value="Aveo">Aveo</option><option value="Beretta">Beretta</option><option value="Blazer">Blazer</option><option value="Camaro">Camaro</option><option value="Captiva">Captiva</option><option value="Cavalier">Cavalier</option><option value="Corvette">Corvette</option><option value="Cruze">Cruze</option><option value="Epica">Epica</option><option value="Evanda">Evanda</option><option value="HHR">HHR</option><option value="Kalos">Kalos</option><option value="Lacetti">Lacetti</option><option value="Lumina">Lumina</option><option value="Malibu">Malibu</option><option value="Matiz">Matiz</option><option value="Nubira">Nubira</option><option value="Orlando">Orlando</option><option value="Rezzo">Rezzo</option><option value="Spark">Spark</option><option value="Suburban">Suburban</option><option value="Tacuma">Tacuma</option><option value="Tahoe">Tahoe</option><option value="TrailBlazer">TrailBlazer</option><option value="Trans Sport">Trans Sport</option><option value="Trax">Trax</option><option value="Volt">Volt</option></select>';
  }
  
  else if (izbranaZnamka == "Chrysler") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="300 C">300 C</option><option value="300 M">300 M</option><option value="Crossfire">Crossfire</option><option value="ES">ES</option><option value="Le Baron">Le Baron</option><option value="Neon">Neon</option><option value="New Yorker">New Yorker</option><option value="Pacifica">Pacifica</option><option value="PT Cruiser">PT Cruiser</option><option value="Saratoga">Saratoga</option><option value="Sebring">Sebring</option><option value="Stratus">Stratus</option><option value="Viper">Viper</option><option value="Vision">Vision</option><option value="Voyager">Voyager</option><option value="Grand Voyager">Grand Voyager</option></select>';
  }

  else if (izbranaZnamka == "Citroen") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="2CV">2 CV</option><option value="Ami">Ami</option><option value="AX">AX</option><option value="Berlingo">Berlingo</option><option value="BX">BX</option><option value="C1">C1</option><option value="C2">C2</option><option value="C3">C3</option><option value="C3 Picasso">C3 Picasso</option><option value="C3 Pluriel">C3 Pluriel</option><option value="C4">C4</option><option value="C4 Aircross">C4 Aircross</option><option value="C4 Cactus">C4 Cactus</option><option value="C4 Picasso">C4 Picasso</option><option value="C4 Grand Picasso">C4 Grand Picasso</option><option value="C5">C5</option><option value="C6">C6</option><option value="C8">C8</option><option value="C-Crosser">C-Crosser</option><option value="C-Elysee">C-Elysee</option><option value="C-Zero">C-Zero</option><option value="CX">CX</option><option value="DS">DS</option><option value="DS3">DS3</option><option value="DS4">DS4</option><option value="DS5">DS5</option><option value="Dyane">Dyane</option><option value="Evasion">Evasion</option><option value="GS">GS</option><option value="GSA">GSA</option><option value="Jumpy">Jumpy</option><option value="LN">LN</option><option value="Nemo">Nemo</option><option value="Saxo">Saxo</option><option value="SM">SM</option><option value="Visa">Visa</option><option value="Xantia">Xantia</option><option value="XM">XM</option><option value="Xsara">Xsara</option><option value="Xsara Picasso">Xsara Picasso</option><option value="ZX">ZX</option></select>';
  }

  else if (izbranaZnamka == "Dacia") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Dokker">Dokker</option><option value="Lodgy">Lodgy</option><option value="Logan">Logan</option><option value="Sandero">Sandero</option><option value="Duster">Duster</option></select>';
  }

  else if (izbranaZnamka == "Daewoo") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Chairman">Chairman</option><option value="Espero">Espero</option><option value="Evanda">Evanda</option><option value="Kalos">Kalos</option><option value="Korando">Korando</option><option value="Lacetti">Lacetti</option><option value="Lanos">Lanos</option><option value="Leganza">Leganza</option><option value="Matiz">Matiz</option><option value="Musso">Musso</option><option value="Nexia">Nexia</option><option value="Nubira">Nubira</option><option value="Racer">Racer</option><option value="Tacuma">Tacuma</option><option value="Tico">Tico</option></select>';
  }

  else if (izbranaZnamka == "Daihatsu") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Applause">Applause</option><option value="Charade">Charade</option><option value="Copen">Copen</option><option value="Cuore">Cuore</option><option value="Feroza">Feroza</option><option value="Freeclimber">Freeclimber</option><option value="Gran Move">Gran Move</option><option value="Materia">Materia</option><option value="Move">Move</option><option value="Rocky">Rocky</option><option value="Sirion">Sirion</option><option value="Terios">Terios</option><option value="Trevis">Trevis</option><option value="YRV">YRV</option></select>';
  }

  else if (izbranaZnamka == "Dodge") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Avenger">Avenger</option><option value="Caliber">Caliber</option><option value="Caravan">Caravan</option><option value="Charger">Charger</option><option value="Durango">Durango</option><option value="Journey">Journey</option><option value="Magnum">Magnum</option><option value="Nitro">Nitro</option><option value="RAM">RAM</option><option value="Stealth">Stealth</option><option value="Viper">Viper</option></select>';
  }

  else if (izbranaZnamka == "Ferrari") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="208">208</option><option value="308">308</option><option value="328">328</option><option value="348">348</option><option value="355">355</option><option value="360">360</option><option value="400">400</option><option value="412">412</option><option value="430">430</option><option value="456">456</option><option value="458">458</option><option value="488">488</option><option value="512">512</option><option value="550 Maranello">550 Maranello</option><option value="575">575</option><option value="599">599</option><option value="612 Scaglietti ">612 Scaglietti </option><option value="California">California</option><option value="Dino">Dino</option><option value="Enzo">Enzo</option><option value="F12berlinetta">F12berlinetta</option><option value="F40">F40</option><option value="F50">F50</option><option value="FF">FF</option><option value="Mondial">Mondial</option><option value="Superamerica">Superamerica</option><option value="Testarossa">Testarossa</option></select>';
  }

  else if (izbranaZnamka == "Fiat") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="1100">1100</option><option value="124">124</option><option value="125">125</option><option value="126">126</option><option value="127">127</option><option value="130">130</option><option value="131">131</option><option value="132">132</option><option value="500">500</option><option value="500C">500C</option><option value="500L">500L</option><option value="500X">500X</option><option value="600">600</option><option value="850">850</option><option value="1500">1500</option><option value="Albea">Albea</option><option value="Argenta">Argenta</option><option value="Barchetta">Barchetta</option><option value="Brava">Brava</option><option value="Bravo">Bravo</option><option value="Campagnola">Campagnola</option><option value="Cinquecento">Cinquecento</option><option value="Coupe">Coupe</option><option value="Croma">Croma</option><option value="Doblo">Doblo</option><option value="Freemont">Freemont</option><option value="Idea">Idea</option><option value="Linea">Linea</option><option value="Marea">Marea</option><option value="Multipla">Multipla</option><option value="Palio">Palio</option><option value="Panda">Panda</option><option value="Punto">Punto</option><option value="Qubo">Qubo</option><option value="Regata">Regata</option><option value="Ritmo">Ritmo</option><option value="Scudo">Scudo</option><option value="Sedici">Sedici</option><option value="Seicento">Seicento</option><option value="Spider">Spider</option><option value="Stilo">Stilo</option><option value="Tempra">Tempra</option><option value="Tipo">Tipo</option><option value="Ulysse">Ulysse</option><option value="Uno">Uno</option><option value="X 1/9">X 1/9</option></select>';
  }

  else if (izbranaZnamka == "Ford") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Aerostar">Aerostar</option><option value="Bronco">Bronco</option><option value="B-Max">B-Max</option><option value="C-Max">C-Max</option><option value="Grand C-Max">Grand C-Max</option><option value="Capri">Capri</option><option value="Cougar">Cougar</option><option value="Ecosport">Ecosport</option><option value="Edge">Edge</option><option value="Escort">Escort</option><option value="Excursion">Excursion</option><option value="Expedition">Expedition</option><option value="Explorer">Explorer</option><option value="Fiesta">Fiesta</option><option value="Focus">Focus</option><option value="Fusion">Fusion</option><option value="Galaxy">Galaxy</option><option value="Granada">Granada</option><option value="GT">GT</option><option value="Ka">Ka</option><option value="Kuga">Kuga</option><option value="Maverick">Maverick</option><option value="Mondeo">Mondeo</option><option value="Mustang">Mustang</option><option value="Orion">Orion</option><option value="Probe">Probe</option><option value="Puma">Puma</option><option value="Ranger">Ranger</option><option value="S-Max">S-Max</option><option value="Scorpio">Scorpio</option><option value="Sierra">Sierra</option><option value="StreetKa">StreetKa</option><option value="Taunus">Taunus</option><option value="Taurus">Taurus</option><option value="Thunderbird">Thunderbird</option><option value="Tourneo">Tourneo</option><option value="Windstar">Windstar</option></select>';
  }

  else if (izbranaZnamka == "Honda") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Accord">Accord</option><option value="Civic">Civic</option><option value="Concerto">Concerto</option><option value="CR-V">CR-V</option><option value="CR-Z">CR-Z</option><option value="CRX">CRX</option><option value="FR-V">FR-V</option><option value="HR-V">HR-V</option><option value="Insight">Insight</option><option value="Integra">Integra</option><option value="Jazz">Jazz</option><option value="Legend">Legend</option><option value="Logo">Logo</option><option value="NSX">NSX</option><option value="Odyssey">Odyssey</option><option value="Pilot">Pilot</option><option value="Prelude">Prelude</option><option value="S2000">S2000</option><option value="Shuttle">Shuttle</option><option value="Stream">Stream</option></select>';
  }

  else if (izbranaZnamka == "Hummer") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="H1">H1</option><option value="H2">H2</option><option value="H3">H3</option></select>';
  }

  else if (izbranaZnamka == "Hyundai") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Accent">Accent</option><option value="Atos">Atos</option><option value="Coupe">Coupe</option><option value="Elantra">Elantra</option><option value="Excel">Excel</option><option value="Galloper">Galloper</option><option value="Getz">Getz</option><option value="Grandeur">Grandeur</option><option value="H-1">H-1</option><option value="H100">H100</option><option value="i10">i10</option><option value="i20">i20</option><option value="i30">i30</option><option value="i40">i40</option><option value="ix20">ix20</option><option value="ix35">ix35</option><option value="ix55">ix55</option><option value="Lantra">Lantra</option><option value="Matrix">Matrix</option><option value="Pony">Pony</option><option value="S-Coupe">S-Coupe</option><option value="Santa Fe">Santa Fe</option><option value="Grand Santa Fe"> Grand Santa Fe</option><option value="Santamo">Santamo</option><option value="Sonata">Sonata</option><option value="Terracan">Terracan</option><option value="Trajet">Trajet</option><option value="Tucson">Tucson</option><option value="Veloster">Veloster</option><option value="XG">XG</option></select>';
  }

  else if (izbranaZnamka == "Jaguar") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Daimler">Daimler</option><option value="MK-II">MK-II</option><option value="E-Type">E-Type</option><option value="F-Type">F-Type</option><option value="S-Type">S-Type</option><option value="X-Type">X-Type</option><option value="XE">XE</option><option value="XF">XF</option><option value="XJ">XJ</option><option value="XJS">XJS</option><option value="XK">XK</option></select>';
  }

  else if (izbranaZnamka == "Jeep") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="CJ">CJ</option><option value="Cherokee">Cherokee</option><option value="Commander">Commander</option><option value="Compass">Compass</option><option value="Grand Cherokee">Grand Cherokee</option><option value="Patriot">Patriot</option><option value="Renegade">Renegade</option><option value="Wagoneer">Wagoneer</option><option value="Willys">Willys</option><option value="Wrangler">Wrangler</option></select>';
  }

  else if (izbranaZnamka == "Kia") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Carens">Carens</option><option value="Carnival">Carnival</option><option value="CeeD">CeeD</option><option value="Cerato">Cerato</option><option value="Clarus">Clarus</option><option value="Elan">Elan</option><option value="Joice">Joice</option><option value="Magentis">Magentis</option><option value="Opirus">Opirus</option><option value="Optima">Optima</option><option value="Picanto">Picanto</option><option value="Pride">Pride</option><option value="Pro_CeeD">Pro_CeeD</option><option value="Retona">Retona</option><option value="Rio">Rio</option><option value="Rocsta">Rocsta</option><option value="Sephia">Sephia</option><option value="Sorento">Sorento</option><option value="Soul">Soul</option><option value="Spectra">Spectra</option><option value="Sportage">Sportage</option><option value="Venga">Venga</option></select>';
  }

  else if (izbranaZnamka == "Lada") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="110">110</option><option value="111">111</option><option value="112">112</option><option value="1300">1300</option><option value="1500">1500</option><option value="Desetka">Desetka</option><option value="Aleko">Aleko</option><option value="wag">Karavan</option><option value="Niva">Niva</option><option value="Nova">Nova</option><option value="Samara">Samara</option></select>';
  }

  else if (izbranaZnamka == "Lamborghini") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Aventador">Aventador</option><option value="Countach">Countach</option><option value="Diablo">Diablo</option><option value="Espada">Espada</option><option value="Gallardo">Gallardo</option><option value="Miura">Miura</option><option value="Murcielago">Murcielago</option></select>';
  }

  else if (izbranaZnamka == "Lancia") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Beta">Beta</option><option value="Dedra">Dedra</option><option value="Delta">Delta</option><option value="Flavia">Flavia</option><option value="Fulvia">Fulvia</option><option value="Kappa">Kappa</option><option value="Lybra">Lybra</option><option value="Musa">Musa</option><option value="Phedra">Phedra</option><option value="Prisma">Prisma</option><option value="Stratos">Stratos</option><option value="Thema">Thema</option><option value="Thesis">Thesis</option><option value="Voyager">Voyager</option><option value="Y">Y</option><option value="Ypsilon">Ypsilon</option><option value="Zeta">Zeta</option></select>';
  }

  else if (izbranaZnamka == "Land Rover") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Defender">Defender</option><option value="Discovery">Discovery</option><option value="Discovery Sport">Discovery Sport</option><option value="Freelander">Freelander</option><option value="Range Rover">Range Rover</option><option value="Range Rover Sport">Range Rover Sport</option><option value="Range Rover Evoque">Range Rover Evoque</option></select>';
  }

  else if (izbranaZnamka == "Lexus") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="CT">CT</option><option value="GS">GS</option><option value="GX">GX</option><option value="IS">IS</option><option value="LFA">LFA</option><option value="LS">LS</option><option value="LX">LX</option><option value="NX">NX</option><option value="RX">RX</option><option value="SC">SC</option></select>';
  }

  else if (izbranaZnamka == "Lotus") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="340R">340R</option><option value="Elan">Elan</option><option value="Elise">Elise</option><option value="Esprit">Esprit</option><option value="Europa">Europa</option><option value="Evora">Evora</option><option value="Excel">Excel</option><option value="Exige">Exige</option><option value="Super seven">Super seven</option></select>';
  }

  else if (izbranaZnamka == "Maserati") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="222">222</option><option value="224">224</option><option value="228">228</option><option value="418">418</option><option value="420">420</option><option value="422">422</option><option value="424">424</option><option value="430">430</option><option value="GT 3200">GT 3200</option><option value="Biturbo">Biturbo</option><option value="Coupe">Coupe</option><option value="Ghibli">Ghibli</option><option value="GranSport">GranSport</option><option value="GranTurismo">GranTurismo</option><option value="GranCabrio">GranCabrio</option><option value="Quattroporte">Quattroporte</option><option value="Shamal">Shamal</option><option value="Spyder">Spyder</option></select>';
  }

  else if (izbranaZnamka == "Maybach") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="57">57</option><option value="62">62</option><option value="Guard">Guard</option><option value="Landaulet">Landaulet</option></select>';
  }

  else if (izbranaZnamka == "Mazda") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="121">121</option><option value="323">323</option><option value="626">626</option><option value="929">929</option><option value="BT-50">BT-50</option><option value="CX-3">CX-3</option><option value="CX-5">CX-5</option><option value="CX-7">CX-7</option><option value="CX-9">CX-9</option><option value="Demio">Demio</option><option value="Mazda2">Mazda2</option><option value="Mazda3">Mazda3</option><option value="Mazda5">Mazda5</option><option value="Mazda6">Mazda6</option><option value="MPV">MPV</option><option value="MX-3">MX-3</option><option value="MX-5">MX-5</option><option value="MX-6">MX-6</option><option value="Premacy">Premacy</option><option value="RX-7">RX-7</option><option value="RX-8">RX-8</option><option value="Tribute">Tribute</option><option value="Xedos 6">Xedos 6</option><option value="Xedos 9">Xedos 9</option></select>';
  }

  else if (izbranaZnamka == "Mercedes-Benz") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="190">190</option><option value="W123">200 - 280 W/T/C 123</option><option value="W124">200 - 500 W/T/C 124</option><option value="W126">260 - 560 W/C 126</option><option value="W140">300 - 600 W/C 140</option><option value="AMG GT">AMG GT</option><option value="A-Razred">A razred</option><option value="B-Razred">B razred</option><option value="C-Razred">C razred</option><option value="CL-Razred">CL razred</option><option value="CLA-Razred">CLA razred</option><option value="CLC-Razred">CLC razred</option><option value="CLK-Razred">CLK razred</option><option value="CLS-Razred">CLS razred</option><option value="E-Razred">E razred</option><option value="G-Razred">G razred</option><option value="GL-Razred">GL razred</option><option value="GLA-Razred">GLA razred</option><option value="GLC-Razred">GLC razred</option><option value="GLE razred">GLE razred</option><option value="GLE coupe">GLE coupe</option><option value="GLK-Razred">GLK razred</option><option value="GLS razred">GLS razred</option><option value="ML-Razred">ML razred</option><option value="R-Razred">R razred</option><option value="S-Razred">S razred</option><option value="SL-Razred">SL razred</option><option value="SLC-Razred">SLC razred</option><option value="SLS-Razred">SLS razred</option><option value="SLK-Razred">SLK razred</option><option value="SLR">SLR razred</option><option value="V-Razred">V razred</option><option value="Vaneo">Vaneo</option><option value="Viano">Viano</option><option value="Vito">Vito</option></select>';
  }

  else if (izbranaZnamka == "Mini") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="-">-</option><option value="Cabrio">Cabrio</option><option value="Clubman">Clubman</option><option value="Countryman">Countryman</option><option value="Coupe">Coupe</option><option value="Paceman">Paceman</option><option value="Roadster">Roadster</option></select>';
  }

  else if (izbranaZnamka == "Mitsubishi") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="3000 GT">3000 GT</option><option value="ASX">ASX</option><option value="Carisma">Carisma</option><option value="Colt">Colt</option><option value="Diamante">Diamante</option><option value="Eclipse">Eclipse</option><option value="Galant">Galant</option><option value="Grandis">Grandis</option><option value="i-MiEV">i-MiEV</option><option value="L 200">L 200</option><option value="L 300">L 300</option><option value="Lancer">Lancer</option><option value="Outlander">Outlander</option><option value="Pajero">Pajero</option><option value="Sigma">Sigma</option><option value="Space Gear">Space Gear</option><option value="Space Runner">Space Runner</option><option value="Space Star">Space Star</option><option value="Space Wagon">Space Wagon</option><option value="Starion">Starion</option></select>';
  }

  else if (izbranaZnamka == "Nissan") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="100 NX">100 NX</option><option value="200 SX">200 SX</option><option value="280 ZX">280 ZX</option><option value="300 ZX">300 ZX</option><option value="350 Z">350 Z</option><option value="370 Z">370 Z</option><option value="Almera">Almera</option><option value="Almera Tino">Almera Tino</option><option value="Altima">Altima</option><option value="Bluebird">Bluebird</option><option value="Cherry">Cherry</option><option value="Cube">Cube</option><option value="Evalia">Evalia</option><option value="GT-R">GT-R</option><option value="Juke">Juke</option><option value="King Cab">King Cab</option><option value="Leaf">Leaf</option><option value="Maxima">Maxima</option><option value="Micra">Micra</option><option value="Murano">Murano</option><option value="Navara">Navara</option><option value="Note">Note</option><option value="Pathfinder">Pathfinder</option><option value="Patrol">Patrol</option><option value="Pick Up">Pick Up</option><option value="Pixo">Pixo</option><option value="Prairie">Prairie</option><option value="Primera">Primera</option><option value="Pulsar">Pulsar</option><option value="Qashqai">Qashqai</option><option value="Quest">Quest</option><option value="Serena">Serena</option><option value="Silvia">Silvia</option><option value="Skyline">Skyline</option><option value="Sunny">Sunny</option><option value="Terrano">Terrano</option><option value="Tiida">Tiida</option><option value="Vanette">Vanette</option><option value="X-Trail">X-Trail</option></select>';
  }

  else if (izbranaZnamka == "Opel") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Adam">Adam</option><option value="Agila">Agila</option><option value="Ampera">Ampera</option><option value="Antara">Antara</option><option value="Ascona">Ascona</option><option value="Astra">Astra</option><option value="Calibra">Calibra</option><option value="Campo">Campo</option><option value="Cascada">Cascada</option><option value="Combo">Combo</option><option value="Commodore">Commodore</option><option value="Corsa">Corsa</option><option value="Diplomat">Diplomat</option><option value="Frontera">Frontera</option><option value="GT">GT</option><option value="Insignia">Insignia</option><option value="Kadett">Kadett</option><option value="Karl">Karl</option><option value="Manta">Manta</option><option value="Meriva">Meriva</option><option value="Mokka">Mokka</option><option value="Monterey">Monterey</option><option value="Monza">Monza</option><option value="Olympia">Olympia</option><option value="Omega">Omega</option><option value="Rekord">Rekord</option><option value="Senator">Senator</option><option value="Signum">Signum</option><option value="Sintra">Sintra</option><option value="Speedster">Speedster</option><option value="Tigra">Tigra</option><option value="Vectra">Vectra</option><option value="Zafira">Zafira</option></select>';
  }

  else if (izbranaZnamka == "Peugeot") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="104">104</option><option value="106">106</option><option value="107">107</option><option value="108">108</option><option value="1007">1007</option><option value="204">204</option><option value="205">205</option><option value="206">206</option><option value="207">207</option><option value="2008">2008</option><option value="208">208</option><option value="3008">3008</option><option value="301">301</option><option value="304">304</option><option value="305">305</option><option value="306">306</option><option value="307">307</option><option value="308">308</option><option value="309">309</option><option value="404">404</option><option value="405">405</option><option value="406">406</option><option value="4007">4007</option><option value="4008">4008</option><option value="407">407</option><option value="5008">5008</option><option value="504">504</option><option value="505">505</option><option value="508">508</option><option value="604">604</option><option value="605">605</option><option value="607">607</option><option value="806">806</option><option value="807">807</option><option value="Bipper">Bipper</option><option value="Expert">Expert</option><option value="iOn">iOn</option><option value="Partner">Partner</option><option value="RCZ">RCZ</option></select>';
  }

  else if (izbranaZnamka == "Pontiac") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Bonneville">Bonneville</option><option value="Fiero">Fiero</option><option value="Firebird">Firebird</option><option value="Trans Am">Trans Am</option><option value="Grand Am">Grand Am</option><option value="Grand Prix">Grand Prix</option><option value="Trans Sport">Trans Sport</option></select>';
  }

  else if (izbranaZnamka == "Porsche") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="356">356</option><option value="911">911</option><option value="912">912</option><option value="914">914</option><option value="924">924</option><option value="928">928</option><option value="944">944</option><option value="959">959</option><option value="968">968</option><option value="Boxster">Boxster</option><option value="Cayenne">Cayenne</option><option value="Cayman">Cayman</option><option value="Carrera GT">Carrera GT</option><option value="Macan">Macan</option><option value="Panamera">Panamera</option></select>';
  }

  else if (izbranaZnamka == "Renault") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="R 4">R 4</option><option value="R 5">R 5</option><option value="R 8">R 8</option><option value="R 9">R 9</option><option value="R 10">R 10</option><option value="R 11">R 11</option><option value="R 12">R 12</option><option value="R 14">R 14</option><option value="R 16">R 16</option><option value="R 18">R 18</option><option value="R 19">R 19</option><option value="R 20">R 20</option><option value="R 21">R 21</option><option value="R 25">R 25</option><option value="R 30">R 30</option><option value="Alpine">Alpine</option><option value="Avantime">Avantime</option><option value="Captur">Captur</option><option value="Clio">Clio</option><option value="Espace">Espace</option><option value="Grand Espace"> Grand Espace</option><option value="Fluence">Fluence</option><option value="Fuego">Fuego</option><option value="Kadjar">Kadjar</option><option value="Kangoo">Kangoo</option><option value="Koleos">Koleos</option><option value="Laguna">Laguna</option><option value="Latitude">Latitude</option><option value="Megane">Megane</option><option value="Modus">Modus</option><option value="Grand Modus"> Grand Modus</option><option value="Safrane">Safrane</option><option value="Scenic">Scenic</option><option value="Grand Scenic"> Grand Scenic</option><option value="Sport Spider">Sport Spider</option><option value="Talisman">Talisman</option><option value="Thalia">Thalia</option><option value="Twingo">Twingo</option><option value="Twizy">Twizy</option><option value="Vel Satis">Vel Satis</option><option value="Wind">Wind</option><option value="Zoe">Zoe</option></select>';
  }

  else if (izbranaZnamka == "Rolls-Royce") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Corniche">Corniche</option><option value="Flying Spur">Flying Spur</option><option value="Ghost">Ghost</option><option value="Park Ward">Park Ward</option><option value="Phantom">Phantom</option><option value="Silver Cloud">Silver Cloud</option><option value="Silver Dawn">Silver Dawn</option><option value="Silver Seraph">Silver Seraph</option><option value="Silver Shadow">Silver Shadow</option><option value="Silver Spirit">Silver Spirit</option><option value="Silver Spur">Silver Spur</option><option value="Wraith">Wraith</option></select>';
  }

  else if (izbranaZnamka == "Rover") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="25">25</option><option value="45">45</option><option value="75">75</option><option value="serija 100:">100</option><option value="serija 200:">200</option><option value="serija 400:">400</option><option value="serija 600:">600</option><option value="serija 800:">800</option><option value="City Rover">City Rover</option><option value="Metro">Metro</option><option value="Montego">Montego</option><option value="Streetwise">Streetwise</option></select>';
  }

  else if (izbranaZnamka == "Saab") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="9-3">9-3</option><option value="9-4x">9-4x</option><option value="9-5">9-5</option><option value="9-7x">9-7x</option><option value="90">90</option><option value="96">96</option><option value="99">99</option><option value="900">900</option><option value="9000">9000</option></select>';
  }

  else if (izbranaZnamka == "Seat") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Alhambra">Alhambra</option><option value="Altea">Altea</option><option value="Arosa">Arosa</option><option value="Cordoba">Cordoba</option><option value="Exeo">Exeo</option><option value="Ibiza">Ibiza</option><option value="Inca">Inca</option><option value="Leon">Leon</option><option value="Marbella">Marbella</option><option value="Malaga">Malaga</option><option value="Mii">Mii</option><option value="Toledo">Toledo</option></select>';
  }

  else if (izbranaZnamka == "Smart") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="city cabrio">city cabrio</option><option value="city coupe">city coupe</option><option value="Crossblade">crossblade</option><option value="forfour">forfour</option><option value="fortwo">fortwo</option><option value="Roadster">roadster</option></select>';
  }

  else if (izbranaZnamka == "Subaru") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="700 SDX">700 SDX</option><option value="Baja">Baja</option><option value="BRZ">BRZ</option><option value="Forester">Forester</option><option value="Impreza">Impreza</option><option value="Justy">Justy</option><option value="Legacy">Legacy</option><option value="Leone">Leone</option><option value="Levorg">Levorg</option><option value="Libero">Libero</option><option value="Outback">Outback</option><option value="SVX">SVX</option><option value="Trezia">Trezia</option><option value="Tribeca">Tribeca</option><option value="Vivio">Vivio</option><option value="WRX STI">WRX STI</option><option value="XT">XT</option><option value="XV">XV</option></select>';
  }

  else if (izbranaZnamka == "Suzuki") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Alto">Alto</option><option value="Baleno">Baleno</option><option value="Capuccino">Capuccino</option><option value="Celerio">Celerio</option><option value="Ignis">Ignis</option><option value="Jimny">Jimny</option><option value="Kizashi">Kizashi</option><option value="Liana">Liana</option><option value="Maruti">Maruti</option><option value="Samurai">Samurai</option><option value="Splash">Splash</option><option value="Swift">Swift</option><option value="SX4">SX4</option><option value="SX4 S-Cross">SX4 S-Cross</option><option value="Vitara">Vitara</option><option value="Grand Vitara"> Grand Vitara</option><option value="Wagon R">Wagon R</option><option value="X-90">X-90</option></select>';
  }

  else if (izbranaZnamka == "Škoda") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="S 105">S 105</option><option value="S 120">S 120</option><option value="Citigo">Citigo</option><option value="Fabia">Fabia</option><option value="Favorit">Favorit</option><option value="Felicia">Felicia</option><option value="Forman">Forman</option><option value="Octavia">Octavia</option><option value="Rapid">Rapid</option><option value="Roomster">Roomster</option><option value="Superb">Superb</option><option value="Yeti">Yeti</option></select>';
  }

  else if (izbranaZnamka == "Toyota") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="4-Runner">4-Runner</option><option value="Auris">Auris</option><option value="Avensis">Avensis</option><option value="Avensis Verso">Avensis Verso</option><option value="Aygo">Aygo</option><option value="Camry">Camry</option><option value="Carina">Carina</option><option value="Celica">Celica</option><option value="Corolla">Corolla</option><option value="Corolla Verso">Corolla Verso</option><option value="FJ">FJ</option><option value="GT 86">GT 86</option><option value="Hi-Lux">Hi-Lux</option><option value="HiAce">HiAce</option><option value="IQ">IQ</option><option value="Land Cruiser">Land Cruiser</option><option value="MR-2">MR-2</option><option value="LiteAce">LiteAce</option><option value="Paseo">Paseo</option><option value="Picnic">Picnic</option><option value="Previa">Previa</option><option value="Prius">Prius</option><option value="RAV4">RAV4</option><option value="Starlet">Starlet</option><option value="Supra">Supra</option><option value="Tercel">Tercel</option><option value="Urban Cruiser">Urban Cruiser</option><option value="Verso">Verso</option><option value="Verso S">Verso S</option><option value="Yaris">Yaris</option><option value="Yaris Verso">Yaris Verso</option></select>';
  }

  else if (izbranaZnamka == "Trabant") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="601">601</option><option value="Cabrio">Cabrio</option></select>';
  }

  else if (izbranaZnamka == "Volvo") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="240">240</option><option value="340">340</option><option value="360">360</option><option value="440">440</option><option value="460">460</option><option value="480">480</option><option value="740">740</option><option value="760">760</option><option value="780">780</option><option value="850">850</option><option value="940">940</option><option value="960">960</option><option value="C30">C30</option><option value="C70">C70</option><option value="S40">S40</option><option value="S60">S60</option><option value="S70">S70</option><option value="S80">S80</option><option value="S90">S90</option><option value="V40">V40</option><option value="V50">V50</option><option value="V60">V60</option><option value="V70">V70</option><option value="V90">V90</option><option value="XC60">XC60</option><option value="XC70">XC70</option><option value="XC90">XC90</option></select>';
  }

  else if (izbranaZnamka == "Volkswagen") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="Amarok">Amarok</option><option value="Beetle">Beetle</option><option value="Bora">Bora</option><option value="Buggy">Buggy</option><option value="Caddy">Caddy</option><option value="California">California</option><option value="Caravelle">Caravelle</option><option value="CC">CC</option><option value="Corrado">Corrado</option><option value="Derby">Derby</option><option value="Eos">Eos</option><option value="Fox">Fox</option><option value="Golf">Golf</option><option value="Golf Plus"> Golf Plus</option><option value="Golf Sportsvan"> Golf Sportsvan</option><option value="CrossGolf"> CrossGolf</option><option value="Hrošč">Hrošč</option><option value="Jetta">Jetta</option><option value="Karmann Ghia">Karmann Ghia</option><option value="Lupo">Lupo</option><option value="Multivan">Multivan</option><option value="Passat">Passat</option><option value="Passat CC">Passat CC</option><option value="Phaeton">Phaeton</option><option value="Polo">Polo</option><option value="CrossPolo"> CrossPolo</option><option value="Santana">Santana</option><option value="Scirocco">Scirocco</option><option value="Sharan">Sharan</option><option value="Shuttle">Shuttle</option><option value="Taro">Taro</option><option value="Tiguan">Tiguan</option><option value="Touareg">Touareg</option><option value="Touran">Touran</option><option value="Transporter">Transporter</option><option value="CrossTouran"> CrossTouran</option><option value="up!">up!</option><option value="Vento">Vento</option><option value="XL1">XL1</option></select>';
  }

  else if (izbranaZnamka == "Wartburg") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="1.3">1.3</option><option value="311">311</option><option value="353">353</option></select>';
  }

  else if (izbranaZnamka == "Zastava") {
    document.getElementById("izbira_modela").innerHTML = '<select name="model" style="width:135px;"><option value="">Izberite model</option><option value="101">101</option><option value="128">128</option><option value="750">750</option><option value="850">850</option><option value="Yugo">Yugo</option><option value="Florida">Yugo Florida</option></select>';
  }

  else {
    var tukajDodajam = document.getElementById("izbira_modela");
    var vnos = document.createElement("option");
    vnos.text = "Izberite model";
    tukajDodajam.add(vnos);
  }

  $('input[type="submit"]').prop('disabled', false);
}

function brisiVseModele() {
  var tmp = document.getElementById("izbira_modela");
  while (tmp.firstChild) {
    tmp.removeChild(tmp.firstChild);
  }
}

function izpisiPodatke() {

  //$('#tabelaIzbraniIzpis tr:last').removeChild();

  var izbira = document.getElementById("izbira_znamke");
  var izbranaZnamka = izbira.options[izbira.selectedIndex].text;

  var izbira2 = document.getElementById("izbira_modela");
  var izbranModel = izbira2.options[izbira2.selectedIndex].text;

  $('#tabelaIzbraniIzpis tr').empty();
  $('#tabelaIzbraniIzpis tr:last').after('<tr><th>ID</th><th>Avtomobil</th><th>Delovna prostornina (ccm)</th><th>Vrsta goriva</th><th>Povprečna poraba</th><th>Število zapisov</th><th>Uporabnik</th><th>Več o zapisu</th></tr>');

  if (izbranaZnamka == "BMW" && izbranModel == "M3") {
    for (var i in podatki) {
      var tmp = podatki[i];
      $('#tabelaIzbraniIzpis tr:last').after('<tr><td>' + tmp.id + '</td><td>' + tmp.avtomobil + '</td><td>' + tmp.delovnaProstornina + '</td><td>' + tmp.vrstaGoriva + '</td><td>' + tmp.povprecnaPoraba + '</td><td>' + tmp.stZapisov + '</td><td>' + tmp.uporabnik + '<td><a href=' + tmp.uporabnik + ' class="info">Poglej več</a></td>' + '</td></tr>');
    }
     $('input[type="submit"]').prop('disabled', true);
  }
  else {
    $('#tabelaIzbraniIzpis tr').empty();
    var opozori = alert("Opozorilo!\nZa izbrano znamko in model avtomobila žal ne obstaja zapis v bazi podatkov!");
  }
}


$(document).ready(function() {

  $('#izbira_znamke').change(function(){
    izbiraZnamke();
  });
});

var podatki = [{
  id: 1,
  avtomobil: 'BMW M3 letnik 2005',
  delovnaProstornina: '3.2',
  vrstaGoriva: 'bencin',
  povprecnaPoraba: '8.0',
  stZapisov: '2',
  uporabnik: 'Admin'
}, {
id: 2,
  avtomobil: 'BMW M3 letnik 2014',
  delovnaProstornina: '3.0',
  vrstaGoriva: 'bencin',
  povprecnaPoraba: '6.9',
  stZapisov: '91',
  uporabnik: 'divjiZajcek77'
}];

var podatkiStatistika = [{
  id: 1,
  avtomobil: 'BMW M3 - letnik 2005',
  datumPolnenja: '11/15/2015',
  vrstaGoriva: 'Bencin 95',
  cenaZaLiter: 1.216,
  iztocenoLitrov: 40.1,
  prevozenoKilometrov: 350.6,
  opombe: '/'
}, {
id: 2,
  avtomobil: 'BMW M3 - letnik 2005',
  datumPolnenja: '11/21/2015',
  vrstaGoriva: 'Bencin 95',
  cenaZaLiter: 1.216,
  iztocenoLitrov: 45.23,
  prevozenoKilometrov: 420.3,
  opombe: '/'
}];

function izbiraObstojecega() {
  //TO-DO za dropdown
  //console.log("izbiram avtomobil za vnos podatkov");
}

function izbiraGoriva() {
  //TO-DO za dropdown
  //console.log("izbiram gorivo");
}

var celotnoPovprecje = 0;

function izracunajPovprecnoPorabo() {

  var sestevekGorivo = 0;
  var sestevekKilometri = 0;
  var stZapisov = 0;
  for (var i in podatkiStatistika) {
    sestevekGorivo += podatkiStatistika[i].iztocenoLitrov;
    sestevekKilometri += podatkiStatistika[i].prevozenoKilometrov;
    stZapisov++;
  }
  var povprecje = sestevekGorivo/sestevekKilometri*100;
  celotnoPovprecje = povprecje;
  $('#statistikaBox').empty();
  $('#statistikaBox').append('<p>Povprečna poraba za ' + podatkiStatistika[0].avtomobil + ' znaša ' + povprecje.toPrecision(4) + ' l/100km.<p>');
}

function izracunajPovprecnoPoraboKalkulator() {

  var sestevekGorivo = 0;
  var sestevekKilometri = 0;
  var stZapisov = 0;
  for (var i in podatkiStatistika) {
    sestevekGorivo += podatkiStatistika[i].iztocenoLitrov;
    sestevekKilometri += podatkiStatistika[i].prevozenoKilometrov;
    stZapisov++;
  }
  var povprecje = sestevekGorivo/sestevekKilometri*100;
  celotnoPovprecje = povprecje;
}  

function izpisiPodatkeStatistika() {
  var izbira = document.getElementById("izbiraAvtomobilaStatistika");
  var izbranModel = izbira.options[izbira.selectedIndex].text;

  

  $('#tabelaIzbraniIzpis tr').empty();

  if (izbranModel == "BMW M3 - letnik 2005") {
    izracunajPovprecnoPorabo();
    $('#tabelaIzbraniIzpis tr:last').after('<tr><th>Številka zapisa</th><th>Datum polnenja</th><th>Vrsta goriva</th><th>Cena za liter</th><th>Iztočeno litrov</th><th>Prevoženo kilometrov</th><th>Opombe</th></tr>');

    for (var i in podatkiStatistika) {
      var tmp = podatkiStatistika[i];
      $('#tabelaIzbraniIzpis tr:last').after('<tr><td>' + tmp.id + '</td><td>' + tmp.datumPolnenja + '</td><td>' + tmp.vrstaGoriva + '</td><td>' + tmp.cenaZaLiter + '</td><td>' + tmp.iztocenoLitrov + '</td><td>' + tmp.prevozenoKilometrov + '</td><td>' + tmp.opombe + '<td></tr>');
    }
  }
  else {
    $('#tabelaIzbraniIzpisStatistika tr').empty();
    var opozorilo = alert("Opozorilo!\nProsimo izberite avtomobil!");
  }

}

function dodajAvtomobil() {
  var opozorilo = alert("Avtomobil uspešno dodan v bazo!");
}

function narediIzracun() {

  var izbira = document.getElementById("izbira_obstojecega");
  var izbranModel = izbira.options[izbira.selectedIndex].text;
  if (izbranModel != "BMW M3 - letnik 2005") var opozorilo = alert("Opozorilo!\nPozabili ste izpolniti vse podatke!")
  else {
    izracunajPovprecnoPoraboKalkulator();
    var cenaGoriva = 1.216;

    var dolzina_poti = document.getElementById("dolzinaPoti").value;

    cenaGoriva = document.getElementById("cena_goriva").value;


    if (dolzina_poti == undefined || cenaGoriva == undefined || dolzina_poti == "" || cenaGoriva == "") var opozorilo = alert("Opozorilo!\nPozabili ste izpolniti vse podatke!")
    else {
      var izracun = cenaGoriva * celotnoPovprecje * dolzina_poti/100;
      var opozorilo = alert("Opozorilo!\nZa vnešeno pot boste glede na vaše trenutno povprečje porabili " + izracun.toPrecision(4) + " eur!");
    }
  }
}

/**
* Car.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

  	brand: {
  		type: 'string',
  		size: 25,
      required: true
  	},
  	model: {
  		type: 'string',
  		size: 25,
      required: true
  	},  
  	volume: {
  		type: 'float',
      required: true
  	}, 	
  	basicFuelType: {
  		type: 'string',
  		size: 15,
      required: true
  	},
  	year: {
  		type: 'int',
      required: true
  	},
    records: {
      collection: 'record',
      via: 'zapis'
    }
  }
};


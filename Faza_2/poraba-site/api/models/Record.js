/**
* Record.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

  	fuelType: {
  		type: 'string',
  		size: 10,
      required: true
  	},
  	pricePerLiter: {
  		type: 'float',
      required: true
  	},  
  	liters: {
  		type: 'float',
      required: true
  	}, 	
  	kilometres: {
  		type: 'float',
      required: true
  	},
    date: {
      type: 'string',
      required: true
    },
  	extra: {
  		type: 'string',
  		size: 800,
  	},
  	owner: {
  		model: 'user',
  	},
    zapis: {
      model: 'car'
    }
  }

};


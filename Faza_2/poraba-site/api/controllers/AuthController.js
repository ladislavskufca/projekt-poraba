var passport = require('passport');

module.exports = {

    _config: {
        actions: false,
        shortcuts: false,
        rest: false
    },

    login: function(req, res) {

        passport.authenticate('local', function(err, user, info) {
            if ((err) || (!user)) {
                //res.alert("Potrebna je prijava!");
                //req.flash('potrebna_prijava', 'Za dostop do te strani morate biti prijavljeni!');
                //res.redirect('http://localhost:1337/niprijave');
                //req.flash('prijavnoIme', 'ojla gost');
                return res.send({
                    message: info.message,
                    user: user
                });
            }
            req.logIn(user, function(err) {
                if (err) res.send(err);
                req.flash('prijavnoIme', 'Odjava');
                res.redirect('http://localhost:1337/');

                /*return res.send({
                    message: info.message,
                    user: user
                });*/

            });

        })(req, res);
    },

    logout: function(req, res) {
        req.logout();
        res.redirect('/');
    }
};
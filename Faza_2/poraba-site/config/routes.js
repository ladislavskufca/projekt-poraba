module.exports.routes = {

 '/': { 
    //view: 'homepage'
    view: 'prva_stran'
  },

  '/iskalnik_vozil': {
    view: 'iskalnik_vozil_rezultati'
  },

  '/vnesi_podatke': {
    view: 'vnesi_podatke'
  },

  '/kalkulator_stroskov': {
    view: 'kalkulator_stroskov'
  },

  '/statistika': {
    view: 'statistika'
  },



//KOMENT 
  //'get /carview': 'CarController.izpisi',
  'get /login': {
       view: 'prijava'
  },

  'get /prijava': {
       view: 'prijava'
  },


  'post /login': 'AuthController.login',

  '/logout': 'AuthController.logout',

  'get /signup': {
    view: 'signup'
  }


  /*'/prijava': {
    view: 'prijava'
  },

  'get /login': {
    view: 'login'
  },

  '/niprijave': {
    view: 'prva_stran_niPrijave'
  },

  '/vnesi_podatke': 'AuthController.login',

  'post /user': 'AuthController.login',

  'post /login': 'AuthController.login',

  '/logout': 'AuthController.logout',

  'get /signup': {
    view: 'prijava'
  }*/
};
